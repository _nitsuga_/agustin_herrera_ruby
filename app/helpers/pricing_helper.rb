module PricingHelper

  def convertToNumber(value)
    return /\d+\.+\d+/.match(value).try(:[], 0).to_f
  end
    
  def convertToInt(value)
    return value.scan(/\d/).join('').to_i
  end

  def convertToBoolean(value)
    return value=="Yes"?true:false
  end
end
