require 'rubygems'
require 'nokogiri'
require 'restclient'
require 'json'

include PricingHelper

class PricingController < ApplicationController


  def index
    i = 1
    idx = 0
    tmpNode = []
    info = []    
    pricing = []
    page = Nokogiri::HTML(RestClient.get("https://www.port-monitor.com/plans-and-pricing"))   
    
    # Iterate over page searching the fields
    monitoringAry = page.css('div.product h2')
    priceAry = page.css('div.product p:not(.billing_options) > a')
    infoAry = page.css('div.product > dl.thin dd')
    
    # infoAry contains the values for check rate, history, multiple and push notifications
    infoAry.each do |node|      
      if i % 4 == 0
        idx += 1
        tmpNode.push(node.text)
        info.push(tmpNode)
        tmpNode = []
      else 
        tmpNode.push(node.text)
      end
      i += 1
    end

    # Once we have all info, add the prices to the pricing array
    idx = 0
    monitoringAry.each do |item|
      pricing.push({
        "monitors" => PricingHelper.convertToInt(item.text), 
        "check_rate" => PricingHelper.convertToInt(info[idx][0]),
        "history" => PricingHelper.convertToInt(info[idx][1]),       
        "multiple_notifications" => PricingHelper.convertToBoolean(info[idx][2]),
        "push_notifications" => PricingHelper.convertToBoolean(info[idx][3]),
        "price" => PricingHelper.convertToNumber(priceAry[idx].text)
      })
      idx += 1
    end

    # pricing contains all the rows from the website
    @pricing = pricing
  end

end
