# Ruby Exercise - Agustin Herrera

Ruby project that retrieves and parses the pricing information from the next page:

https://www.port-monitor.com/plans-and-pricing

## Getting Started

To run this project you will have to do:
```
rails server
```
Once the server is executing, will exist an endpoint **localhost:3000/api/pricing** 
The output will be like this:

```ruby
[
    {
        monitors: 10,
        check_rate: 60,
        history: 12,
        multiple_notifications: true,
        push_notifications: true,
        price: 4.54
    },
    {
        monitors: 20,
        check_rate: 60,
        history: 12,
        multiple_notifications: true,
        push_notifications: true,
        price: 8.29
    },    
]
```

### Prerequisites

Before executing the server, you need update your **gems**

```
bundle install
```

## Author

**Agustin Herrera** - *Senior FrontEnd Engineer* - [web](http://agustinherrera.es)
