json.array! @pricing do |post|
  json.monitors post['monitors']
  json.check_rate post['check_rate']
  json.history post['history']
  json.multiple_notifications post['multiple_notifications']
  json.push_notifications post['push_notifications']
  json.price post['price']
end